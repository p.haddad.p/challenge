# Start

	1- docker-compose build
	2- docker-compose up

## Front & Back

1. Client: http://localhost:3000
2. Service:  http://localhost:5000

### Apis

	✓ {/api/articles, GET}
		Lista todos los registros menos los borrados
	✓ {/api/articles?all=true, GET}
		Lista todos los registros incluido los borrados
	✓ {/api/articles/pull, GET}
		Borra todos los datos y hace un pull a la api, carga todo de nuevo
	✓ {/api/articles/:id, GET}
		obtiene un registro por su id
	✓ {/api/articles/:id, DELETE}
		Marca el registro como borrado
	✓ {/api/articles/all, DELETE}
		Borra todos los registros
	✓ {/api/articles, POST}
		inserta un registro
	✓ {/api/articles, PUT}
		actualiza un registro


## TEST

Test endpoints, casos importacion de datos de api

	cd server
	npm install

## Test Crud Api

npm run test:e2e article.e2e-spec.ts
npm run test:e2e article.crud.e2e-spec.ts

