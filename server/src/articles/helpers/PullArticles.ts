import { HttpService } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AxiosResponse } from 'axios'
import { ArticleHelper } from './articles'

import { map } from 'rxjs/operators';

export class PullArticles {
    constructor() { }
    get(articlesService): Observable<AxiosResponse<any>> {
        const httpService = new HttpService();
        return httpService.get('http://hn.algolia.com/api/v1/search_by_date?query=nodejs').pipe(
            map(response => {
                const articleHelper = new ArticleHelper();
                articlesService.saveMany(articleHelper.mapArticles(response.data["hits"]))
                return response.data
            }),
        );;
    }
}


