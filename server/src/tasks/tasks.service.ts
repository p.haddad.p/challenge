import { Injectable, Logger, HttpService } from '@nestjs/common';
import { Interval } from '@nestjs/schedule';

import { ArticlesService } from '../articles/articles.service';
import { ArticleHelper } from '../articles/helpers/articles'

@Injectable()
export class TasksService {
  constructor(private articlesService: ArticlesService, private httpService: HttpService) { }
  private readonly logger = new Logger(TasksService.name);
  private readonly articleHelper = new ArticleHelper();


  @Interval(60 * 60 * 1000)
  handleInterval() {
    this.pullDataFromApi();
    this.logger.debug('Called every 1 hour');
  }

  pullDataFromApi() {
    const res = this.httpService.get('http://hn.algolia.com/api/v1/search_by_date?query=nodejs');
    res.subscribe((response) => {
      this.articlesService.saveMany(this.articleHelper.mapArticles(response.data["hits"]))
    });
  }

}
