import { useState, useEffect } from 'react';
import FeedService from './services/feeds'
import mapFeeds from './services/helper/feed.helper'
import FeedList from './components/feed/feed.list'
import Loader from './components/loader/loader'
import './app.css'
function App() {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [feeds, setFeeds] = useState([]);

  const handleDelete = (index: any) => {
    setFeeds([
      ...feeds.slice(0, index),
      ...feeds.slice(index + 1)
    ])
  }

  useEffect(() => {
    setError(false);
    try {
      setLoading(true);
      FeedService.getFeeds().then((res) => {
        setFeeds(mapFeeds(res.data));
        setLoading(false);
      })
        .catch((error) => {
          setLoading(false);
        })
    } catch (error) {

      setError(true);
      setLoading(false);
    }
  }, []);

  const header = { title: "HN Feed", subtitle: "We <3 hacker news!" }

  return (
    <div>
      <header>
        <h1>{header.title}</h1>
        <h2>{header.subtitle} </h2>
      </header>
      <main>
        {(!loading && !error) ?
          feeds.length ? <FeedList onDelete={handleDelete} feeds={feeds}></FeedList> : <div className="messageBox">No se encontraron datos.</div>
          : <Loader></Loader>}

        {(!loading && error) ? <div className="messageBox">Error a al obtener datos</div> : null}
      </main>
    </div>
  )
}


export default App;
