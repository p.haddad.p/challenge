import axios from 'axios';

const API = {
    getFeeds: () => axios.get('/api/articles'),
    deleteFeeds: (id) => axios.delete(`/api/articles/${id}`)
}

export default API;
