import moment from 'moment';

const todayDate = new Date();
const yesterdayDate = new Date();
yesterdayDate.setDate(yesterdayDate.getDate() - 1);

moment.locale('en', {
    calendar: {
        lastDay: '[Yesterday]',
    }
});

function customDate(date) {
    return moment(date).format("YYYYMMDD");
}

const parseDate = (date) => {
    if (customDate(date) === customDate(todayDate)) {
        return moment(date).format('h:mm a');
    }

    if (customDate(date) === customDate(yesterdayDate)) {
        return moment(date).calendar();
    }

    if (moment(date).format("YYYY") === moment(todayDate).format("YYYY")
    ) {
        return moment(date).format("MMM D");
    }
}

const mapFeeds = (feeds) => {
    return feeds.map(feed => {
        return {
            ...feed,
            id: feed._id,
            created_at: parseDate(feed.hit.created_at),
            title: feed.hit.story_title || feed.hit.title,
            url: feed.hit.story_url || feed.hit.url,
            author: feed.hit.author,
        }
    }).filter((feed) => { return feed.title })
}
export default mapFeeds;