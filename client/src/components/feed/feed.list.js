
import React from 'react';
import Feed from './feed';
import './feed.list.css'

function FeedList({ feeds, onDelete }) {
    return (
        <ul className="FeedList">
            {feeds.map((feed, index) => (
                <Feed
                    key={feed.id}
                    {...feed}
                    onDelete={() => onDelete(index)}
                />
            ))}
        </ul>
    )
}

export default FeedList;
