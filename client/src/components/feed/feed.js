import PropTypes from 'prop-types';
import { useState } from 'react';
import FeedService from '../../services/feeds'
import Loader from '../loader/loader'
import deleteIcon from '../../icons/delete.png'
import './feed.css'
function Feed(props) {
    const [loading, setLoading] = useState(false);

    const handleDelete = (e) => {
        e.preventDefault();
        try {
            setLoading(true);
            FeedService.deleteFeeds(props.id).then((res) => {
                setLoading(false);
                props.onDelete()
            }).catch((error) => {
                setLoading(false);
            })
        } catch (error) {
            setLoading(false);
        }
    };

    return (
        <li className="Feed">
            <a href={props.url} className={!props.url ? "noPointer" : ""} target="_blanck" >
                <p className="title">{props.title}<span className="author"> -{props.author}-</span></p>
                <p className="date">{props.created_at}</p>
                {!loading ?
                    <img className="circle" alt="" onClick={handleDelete} src={deleteIcon} width="75" />
                    : <Loader type="inline" ></Loader>}
            </a>
        </li>);
}

Feed.propTypes = {
    id: PropTypes.string,
    title: PropTypes.string,
    author: PropTypes.string,
    created_at: PropTypes.string,
};
export default Feed;





